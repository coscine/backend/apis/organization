﻿using Coscine.ApiCommons;
using Coscine.Database.Models;
using Coscine.Metadata;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Collections.Generic;
using VDS.RDF;
using Newtonsoft.Json;
using Coscine.Api.Organization.ReturnObjects;

namespace Coscine.Api.Organization.Controllers
{
    /// <summary>
    /// This controller represents the actions which can be taken with a organization object.
    /// </summary>
    [Authorize]
    public class OrganizationController : Controller
    {

        private readonly Authenticator _authenticator;
        private readonly RdfStoreConnector _rdfStoreConnector;
        private readonly ExternalIdModel _externalIdModel;

        /// <summary>
        /// Constructor for the organization controller.
        /// </summary>
        public OrganizationController()
        {
            _authenticator = new Authenticator(this, Program.Configuration);
            _rdfStoreConnector = new RdfStoreConnector(Program.Configuration.GetStringAndWait("coscine/local/virtuoso/additional/url"));
            _externalIdModel = new ExternalIdModel();
        }

        /// <summary>
        /// Returns all organizations that match the provided criterea.
        /// </summary>
        /// <param name="member">Filtering organizations that the user is member of.</param>
        /// <param name="filter">Searchterm to filter the results by.</param>
        /// <returns>JSON with a List of the displaynames and urls.</returns>
        [HttpGet("[controller]")]
        public ActionResult<WrapperObject> Index([FromQuery]int member = 0, [FromQuery]string filter = null)
        {
            var user = _authenticator.GetUser();
            var externalIds = _externalIdModel.GetAllWhere((externalId) => externalId.UserId == user.Id);
            var externalIdList = new List<string>();
            foreach (var externalId in externalIds)
            {
                externalIdList.Add(externalId.ExternalId1);
            }

            var organizations = JsonConvert.DeserializeObject<IEnumerable<Uri>>(
                Program.Configuration.GetStringAndWait("coscine/local/organizations/list", 
                    "['https://ror.org/', 'https://ror.org/04xfq0f34']")
            );
            var externalOrganizations = externalIds.Select((externalId) => externalId.Organization);

            var resultSet = new List<Triple>();
            foreach (var orgGraph in organizations)
            {
                resultSet.AddRange(_rdfStoreConnector.GetTriples(orgGraph, null, filter, member, externalIdList));
            }
            foreach (var externalOrganization in externalOrganizations)
            {
                resultSet.AddRange(_rdfStoreConnector.GetOrganizationByEntityId(externalOrganization));
            }
            resultSet = resultSet.Distinct().ToList();

            return Ok(GetObjectForTripleList(resultSet));
        }

        /// <summary>
        /// Returns all labels and urls from the ror graph.
        /// </summary>
        /// <returns>JSON with all labels and urls.</returns>
        [HttpGet("[controller]/-/ror")]
        public ActionResult<WrapperObject> GetROR([FromQuery]string filter)
        {
            return Ok(GetObjectForTripleList(_rdfStoreConnector.GetTriples(new Uri("https://ror.org/"), null, filter)));
        }

        /// <summary>
        /// Returns true if the current user is a member of the given organization.
        /// </summary>
        /// <param name="url">Url of the organization.</param>
        /// <returns>JSON with the result.</returns>
        [HttpGet("[controller]/-/isMember/{url}")]
        public ActionResult<IsMemberObject> IsMember(string url)
        {
            var user = _authenticator.GetUser();
            var externalIds = _externalIdModel.GetAllWhere((externalId) => externalId.UserId == user.Id);
            var externalIdList = new List<string>();
            foreach (var externalId in externalIds)
            {
                externalIdList.Add(externalId.ExternalId1);
            }
            var externalOrganizations = externalIds.Select((externalId) => externalId.Organization);
            var triples = new List<Triple>();
            foreach (var externalOrganization in externalOrganizations)
            {
                triples.AddRange(_rdfStoreConnector.GetOrganizationByEntityId(externalOrganization));
            }

            return Ok(new IsMemberObject { 
                IsMember = _rdfStoreConnector.GetTriples(new Uri(Uri.UnescapeDataString(url)), null, null, 1, externalIdList).Any()
                    || triples.Any((triple) => triple.Subject.ToString() == Uri.UnescapeDataString(url))
            });
        }

        /// <summary>
        /// Returns a list of organizations in which the current user is a member.
        /// </summary>
        /// <returns>JSON with the result.</returns>
        [HttpGet("[controller]/-/isMember")]
        public ActionResult<WrapperObject> IsMember()
        {
            var user = _authenticator.GetUser();
            var externalIds = _externalIdModel.GetAllWhere((externalId) => externalId.UserId == user.Id);
            var externalIdList = new List<string>();
            foreach (var externalId in externalIds)
            {
                externalIdList.Add(externalId.ExternalId1);
            }
            var externalOrganizations = externalIds.Select((externalId) => externalId.Organization);

            var triples = _rdfStoreConnector.GetTriples(null, null, null, 1, externalIdList).ToList();
            foreach (var externalOrganization in externalOrganizations)
            {
                triples.AddRange(_rdfStoreConnector.GetOrganizationByEntityId(externalOrganization));
            }
            triples = triples.Distinct().ToList();

            return Ok(
                GetObjectForTripleList(triples, true)
            );
        }

        /// <summary>
        /// Returns a specific organization.
        /// </summary>
        /// <param name="url">Url of the organization.</param>
        /// <returns>JSON with the organization</returns>
        [HttpGet("[controller]/{url}")]
        public ActionResult<WrapperObject> GetOrganization(string url)
        {
            var unescapedUrl = Uri.UnescapeDataString(url);
            return Ok(GetObjectForTripleList(_rdfStoreConnector.GetLabelForSubject(new Uri(unescapedUrl))));
        }

        /// <summary>
        /// Converts the organization triples to return objects.
        /// </summary>
        /// <param name="list">The list of triples to be converted into <see cref="OrganizationObject"/>s.</param>
        /// <param name="fetchEmail">Optional boolean flag indicating whether to fetch the organization's email. Default is false.</param>
        /// <returns>A <see cref="WrapperObject"/> containing the list of converted <see cref="OrganizationObject"/>s.</returns>
        private WrapperObject GetObjectForTripleList(IEnumerable<Triple> list, bool fetchEmail = false)
        {
            var wrapperObject = new WrapperObject();
            foreach (var triple in list)
            {
                var organization = new OrganizationObject
                {
                    DisplayName = triple.Object.ToString(),
                    Url = triple.Subject.ToString(),
                    Email = fetchEmail ? _rdfStoreConnector.GetOrganizationEmailByRorUrl(triple.Subject.ToString()) : null
                };
                wrapperObject.Data.Add(organization);
            }
            return wrapperObject;
        }
    }
}
