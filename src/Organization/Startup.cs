﻿using Coscine.ApiCommons;

namespace Coscine.Api.Organization
{
    /// <summary>
    /// Standard Startup class.
    /// </summary>
    public class Startup : AbstractStartup
    {
        /// <summary>
        /// Standard Startup constructor.
        /// </summary>
        public Startup()
        {

        }
    }
}
