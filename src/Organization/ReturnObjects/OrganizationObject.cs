﻿namespace Coscine.Api.Organization.ReturnObjects
{
    /// <summary>
    /// Information about an Organization
    /// </summary>
    public class OrganizationObject
    {
        /// <summary>
        /// Name of the Organization
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// ROR Id of the Organization
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// Optional: Contact E-Mail
        /// </summary>
        public string Email { get; set; }
    }
}
