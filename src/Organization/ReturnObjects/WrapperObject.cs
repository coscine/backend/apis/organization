﻿using System.Collections.Generic;

namespace Coscine.Api.Organization.ReturnObjects
{
    /// <summary>
    /// Wrapping the request (to stay compatible to the old implementation)
    /// </summary>
    public class WrapperObject
    {
        /// <summary>
        /// Wrapper list to stay compatible to the old implementation
        /// </summary>
        public List<OrganizationObject> Data { get; set; } = new List<OrganizationObject>();
    }
}
