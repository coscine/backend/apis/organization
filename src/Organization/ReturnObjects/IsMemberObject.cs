﻿namespace Coscine.Api.Organization.ReturnObjects
{
    /// <summary>
    /// Object for representing the isMember relationship
    /// </summary>
    public class IsMemberObject
    {
        /// <summary>
        /// Is Member of requested organization
        /// </summary>
        public bool IsMember { get; set; }
    }
}
